# frontend v 0.0.156a

Run the following command to restore dependencies to `vendor/` directory:

    dep ensure --vendor-only
