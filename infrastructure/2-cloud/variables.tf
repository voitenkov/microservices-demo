variable "token" {
  type        = string
  description = "YC IAM token"
}

variable "cloud_id" {
  type        = string
  description = "YC Cloud ID"
}